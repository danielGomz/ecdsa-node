const secp = require("ethereum-cryptography/secp256k1");
const { toHex } = require("ethereum-cryptography/utils");



/**

    Generates a random private key and its corresponding public key using the secp256k1 elliptic curve algorithm.
    @returns {Array} An array containing the generated private key and public key.
    */
const generateKeys = () => {
    // Generate a random private key
    const privateKey = secp.secp256k1.utils.randomPrivateKey();

    // Derive the corresponding public key from the private key
    const publicKey = secp.secp256k1.getPublicKey(privateKey);

    // Print the generated keys to the console
    console.log('\n* Private key:', toHex(privateKey),
        '\n* Public key:', toHex(publicKey));

    // Return the keys as an array
    return [privateKey, publicKey];
}

generateKeys()